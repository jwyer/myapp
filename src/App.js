import React,  { Component } from 'react';
import Submission from './Components/button.js'
import Todo from './Components/todos.component.jsx'
import './App.css';

class App extends Component {
  constructor(){
    super();

    // this.handleSubmit = this.handleSubmit.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.deleteAll = this.deleteAll.bind(this)
    this.delete = this.delete.bind(this)
    this.update = this.update.bind(this)

    this.state ={
     todos:['finish this component'],
     todoField:'',
     changeTodo:'',

     toggle:false,
     updateToggle: false
    };

  }

  toggleN=() =>{
      this.setState(prevState => ({
      toggle: !prevState.toggle
    }));

  }
  handleChange(event){

    event.persist()
    // https://www.duncanleung.com/blog/2017-08-14-fixing-react-warnings-synthetic-events-in-setstate/

     this.setState({
      [event.target.name]: event.target.value
      })

  }

  handleSubmit =(e)=>{
    e.preventDefault();

    const todo = this.state.todoField.trim();

    this.state.todos.includes(todo) ?
        alert("This todo already exists") :
    this.setState(prevState=>({
      todos: prevState.todos.concat(todo),
      todoField: ""
    }));


  }
  deleteAll(){
    this.setState(()=>({
      todos:[]
    }))
  }
  delete(i){

    this.setState((prevState) =>(
    {
      todos: prevState.todos.filter( n =>  n !== i )
    }))

  }

  update(e,i){
    e.preventDefault();


    this.setState( prevState =>({
      todos: prevState.todos.map(n => {
        if(n === i){
          n = this.state.changeTodo

        }return n;
      }),
      changeTodo: '',
    }))
    console.log(this.state.todos)
  }


  render(){
    const toggle = this.state.toggle;
    const cname = toggle ? "nightmode" : "daylight";
  return (
          <div className={`whole ${cname}`}>
            <button  onClick={this.toggleN}>{this.state.toggle ? "LightOn" : "LightOff"} </button>

            <ul>

             {
              this.state.todos.map( t =>
              <>
              <div>
                <li key={t.toString()}
                  style={{display: "inline-block"}}>

                  {t}
                  <Todo
                    delete={this.delete}
                    update={this.update}
                    handleChange={this.handleChange}
                    t={t}
                    value={this.state.changeTodo}
                    updateToggle={this.state.updateToggle}
                  />
                </li>

              </div>

              </>
              )
            }
            </ul>
            <Submission
              value={this.state.todoField}
              handleChange={this.handleChange}
              handleSubmit={this.handleSubmit}
              deleteAll = {this.deleteAll}

            />
          {  /*<Submission onSubmit={(e)=>this.handleSubmit(e)}>
              <input type="text" placeholder="Some Thing goes here" value={this.state.value} onChange={(e)=>this.handleChange(e)} required/>

              <input type="submit" value="Submit" />
            </Submission>
            <button className="DeleteAll" onClick={()=> this.deleteAll()}> Delete All </button>*/}
          </div>

  );
}
}

export default App;
