import React, { Component } from 'react';

class Todo extends Component{
  constructor(){
    super();
    this.state ={
      updateToggle: false,

    }
      this.updateToggle = this.updateToggle.bind(this)

  }



    updateToggle(e){
      e.preventDefault()
      this.setState( prevState=>({
        updateToggle: !prevState.updateToggle
      }))
  }

render(){
  return (<>
      <form onSubmit={ (e) => {
        this.props.update(e,this.props.t)} }>
        <button
          className="Delete"
          onClick={()=>{this.props.delete(this.props.t)}}>
            Delete
        </button>

          { this.state.updateToggle ?
          <input type="submit" value="Update"/>:
          <button type="button" onClick={this.updateToggle}> Change </button>
          }
        {
          this.state.updateToggle &&
          <input type="text"
            name="changeTodo"
            placeholder="beatings will resume until morale improves"
            onChange={(e) => this.props.handleChange(e)}
            value={this.props.value}
            required/>
        }

      </form>
      </>)
  }
}
export default Todo;
