import React from 'react';

const Submission = function(props){

  return(
    <>
    <form onSubmit={props.handleSubmit}>
      <input name="todoField" type="text" placeholder="Some Thing goes here"  onChange={(e) => props.handleChange(e)} value={props.value}required/>
      <input type="submit" value="Submit" />
      </form>
      <button className="DeleteAll" onClick={()=> props.deleteAll()}> Delete All </button>
     </>
  )
}


export default Submission;
